import { Route, Switch } from "react-router-dom";
import Cart from "../components/Cart";
import Home from "../pages/Home";

export default function Routes({ globalUser, setGlobalUser }) {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/cart">
        <Cart />
      </Route>
    </Switch>
  );
}
