import Routes from "./routes";
import { Container } from "./styles";

function App() {
  return (
    <Container>
      <Routes />
    </Container>
  );
}

export default App;
