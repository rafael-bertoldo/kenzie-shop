import { useSelector } from "react-redux";
import { Container } from "../../pages/Home/styles";
import Product from "../Product";

const Products = () => {
  const products = useSelector((store) => store.products);

  return (
    <Container>
      {products.map((product) => (
        <Product key={product.id} product={product} />
      ))}
    </Container>
  );
};

export default Products;
