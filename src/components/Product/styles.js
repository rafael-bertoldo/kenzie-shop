import styled from "styled-components";

export const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 350px;
  height: 700px;
  max-width: 700px;
  max-height: 500px;
  margin-top: 25px;
  border: 2px solid black;
  margin: 16px;
  border-radius: 16px;
  box-shadow: 4px 4px 4px 4px rgba(0, 0, 0, 0.6);

  img {
    max-width: 300px;
    max-height: 300px;
  }

  button {
    margin-top: 20px;
    margin-bottom: 20px;
    background: rgba(0, 0, 265, 0.6);
    border: none;
    padding: 15px;
    border-radius: 5px;
    font-weight: bold;
    text-transform: uppercase;
    border: 2px solid rgb(0, 0, 265);
  }

  button:hover {
    background-color: #fff;
    cursor: pointer;
  }
`;
