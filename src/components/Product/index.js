import { useDispatch } from "react-redux";
import {
  addToCartThunk,
  removeFromCartThunk,
} from "../../store/modules/Cart/thunks";
import { CardContainer } from "./styles";

const Product = ({ product, isRemovable = false, idRandom }) => {
  const dispatch = useDispatch();
  const { id, name, price, img } = product;

  const handleRemove = (n) => {
    dispatch(removeFromCartThunk(n));
  };

  const handleAdd = (item) => {
    dispatch(addToCartThunk(item));
  };

  return (
    <CardContainer>
      <h2>{name}</h2>
      <h3>RS {price},00</h3>
      <img src={img} alt="img" />

      {isRemovable ? (
        <button onClick={() => handleRemove(id)}>remover do carrinho</button>
      ) : (
        <button onClick={() => handleAdd(product)}>
          adicionar ao carrinho
        </button>
      )}
    </CardContainer>
  );
};

export default Product;
