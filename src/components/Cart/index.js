import { Link } from "react-router-dom";
import Product from "../Product";
import { FaHome } from "react-icons/fa";
import { Container, InfoContainer, ProductsContainer } from "./styles";
import { useEffect, useState } from "react";

const Cart = () => {
  const [cart, setCart] = useState([]);
  useEffect(() => {
    setCart(JSON.parse(localStorage.getItem("@KenzieShop:cart")) || []);
  });

  return (
    <Container>
      <div>
        <Link to="/">
          <FaHome size={64} />
        </Link>
      </div>
      <InfoContainer>
        <h1>Meu Carrinho</h1>
        <p>
          valor a pagar: R$
          <span>
            {cart.reduce((ant, atu) => {
              return atu.price + ant;
            }, 0)}
            ,00
          </span>
        </p>
      </InfoContainer>
      <ProductsContainer>
        {cart.map((product) => {
          return <Product key={product.id} product={product} isRemovable />;
        })}
      </ProductsContainer>
    </Container>
  );
};
export default Cart;
