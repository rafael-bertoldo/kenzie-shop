import { createStore, combineReducers, applyMiddleware } from "redux";

import productsReducer from "./modules/Products/reducers";
import cartReducer from "./modules/Cart/reducers";
import thunk from "redux-thunk";

const reducers = combineReducers({
  products: productsReducer,
  cart: cartReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
