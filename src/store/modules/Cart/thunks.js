import { addToCart, removeFromCart } from "./actions";

export const addToCartThunk = (product) => {
  return (dispatch) => {
    const list = JSON.parse(localStorage.getItem("@KenzieShop:cart")) || [];
    if (list.find((elt) => elt.id === product.id)) {
      alert("produto já adicionado");
    } else {
      list.push(product);
      localStorage.setItem("@KenzieShop:cart", JSON.stringify(list));
      dispatch(addToCart(product));
    }
  };
};

export const removeFromCartThunk = (id) => (dispatch, getStore) => {
  const { cart } = getStore();
  const newList = cart.filter((product) => product.id !== id);
  localStorage.setItem("@KenzieShop:cart", JSON.stringify(newList));
  dispatch(removeFromCart(newList));
};
