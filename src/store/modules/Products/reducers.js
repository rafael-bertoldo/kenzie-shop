const defaultState = [
  {
    id: 1,
    name: "SNES",
    price: 1000,
    img: "https://s2.glbimg.com/pc9JqplhbAyfacIvS3PPygMoM6A=/0x600/s.glbimg.com/po/tt2/f/original/2015/11/27/snes.jpg",
  },
  {
    id: 2,
    name: "Nintendo 64",
    price: 2500,
    img: "https://s2.glbimg.com/PLK5WTBnpcwO_6Tn1WDOcMliUL8=/0x0:695x391/984x0/smart/filters:strip_icc()/s.glbimg.com/po/tt2/f/original/2016/07/15/top10n64.jpg",
  },
  {
    id: 3,
    name: "Mega Drive",
    price: 4000,
    img: "https://dtzhc0rw447zs.cloudfront.net/Custom/Content/Products/01/88/0188_console-mega-drive-dois-joysticks-cartao-sd-com-22-jogos-expansivel-ate-594-jogos-995040481823_m3_637396511314788797.jpg",
  },
  {
    id: 4,
    name: "Sega Saturno",
    price: 800,
    img: "https://blogtectoy.com.br/wp-content/uploads/2017/05/saturn_capa.jpg",
  },
  {
    id: 5,
    name: "Atari",
    price: 1900,
    img: "https://cdn6.campograndenews.com.br/uploads/noticias/2020/03/10/ioefz7pqfays.jpg",
  },
  {
    id: 6,
    name: "Master System",
    price: 500,
    img: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Sega-Master-System-Set.png/1280px-Sega-Master-System-Set.png",
  },
  {
    id: 7,
    name: "Play Station 1",
    price: 1000,
    img: "http://s2.glbimg.com/j1Yjuf-MEphxEN6WmgUuebgDDmY=/695x0/s.glbimg.com/po/tt2/f/original/2014/12/03/curiosidades-20-anos-playstation-one1.jpg",
  },
  {
    id: 8,
    name: "Play Station 2",
    price: 2500,
    img: "https://s2.glbimg.com/xPyxAPKYcA9I-1oZTO4cyvHGpXI=/0x0:695x390/984x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2020/q/E/zZjJhTRlqeGY8aX3sg7w/playstation-2-ps2-20-anos-fatos-curiosos-console-sony.jpg",
  },
  {
    id: 9,
    name: "Dynavision",
    price: 280,
    img: "https://4.bp.blogspot.com/-y5MeQKHH5_0/T39aQ9vO_qI/AAAAAAAAAFw/7xSEPprs9l8/s1600/dynavision+extreme.jpg",
  },
];

const productsReducer = (state = defaultState, action) => {
  return state;
};

export default productsReducer;
