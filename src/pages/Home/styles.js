import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;

  a {
    display: flex;
    align-items: flex-end;
    margin-top: 15px;
    text-decoration: none;
    width: 8%;

    div {
      width: 30px;
      height: 30px;
      text-align: center;
      line-height: 30px;
      font-weight: bold;
      background-color: #f00;
      color: #fff;
      border-radius: 50%;
    }
  }
`;
