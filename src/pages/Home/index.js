import { Link } from "react-router-dom";
import Products from "../../components/Products";
import { Container } from "./styles";
import { FaOpencart } from "react-icons/fa";
import { useSelector } from "react-redux";
const Home = () => {
  const cart = useSelector((store) => store.cart);
  return (
    <Container>
      <Link to="/cart">
        <FaOpencart size={64} />
        {cart.length ? <div>{cart.length}</div> : null}
      </Link>
      <Products />
    </Container>
  );
};

export default Home;
